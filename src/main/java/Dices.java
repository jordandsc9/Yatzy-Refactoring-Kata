import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Dices {

    @Setter(AccessLevel.NONE)
    private List<Integer> dice;
    private int[] tallies = new int[6];

    public Dices (int... dices){
        int[] slice = Arrays.copyOfRange(dices,0, 5);
        Arrays.sort(slice);
        dice = Arrays
            .stream(slice)
            .boxed()
            .collect(Collectors.toUnmodifiableList());
        talliesCount();
    }

    public void talliesCount() {
        tallies[dice.get(0) - 1] += 1;
        tallies[dice.get(1) - 1] += 1;
        tallies[dice.get(2) - 1] += 1;
        tallies[dice.get(3) - 1] += 1;
        tallies[dice.get(4) - 1] += 1;
    }
}