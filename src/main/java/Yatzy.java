import java.util.*;
import java.util.stream.Collectors;

public class Yatzy {

    private Dices dices;

    public Yatzy(int... rolls){
        dices = new Dices(rolls);
    }

    public int chance() {
        return dices.getDice().stream().reduce(0, Integer::sum);
    }

    public int yatzy() {
        return dices.getDice().stream().allMatch(dices.getDice().get(0)::equals) ? 50 : 0;
    }

    public int ones() {
        return genericScoreByNumber(1);
    }

    public int twos() {
        return genericScoreByNumber(2);
    }

    public int threes() {
        return genericScoreByNumber(3);
    }

    public int fours() {
        return genericScoreByNumber(4);
    }

    public int fives() {
        return genericScoreByNumber(5);
    }

    public int sixes() {
        return genericScoreByNumber(6);
    }

    private  int genericScoreByNumber(int number) {
        return dices.getDice().stream().filter(i -> i == number).mapToInt(i -> i).sum();
    }

    public int scorePair() {
        return getSumOfGivenPairNumber(2);
    }

    public int getSumOfGivenPairNumber(int nbrOfPair){
        int count = 0;
        int max = 0;
        for (int i : dices.getTallies()) {
            if(i >= nbrOfPair){
                max = Math.max(max, (count+1) * nbrOfPair);
            }
            count+=1;
        }
        return max;
    }

    public int twoPair() {
        return getSumOfMaxPair();
    }

    private int getSumOfMaxPair() {
        int sum = 0;
        for(int i = dices.getTallies().length - 1; i > 0; i--){
            if (dices.getTallies()[i-1] >= 2){
                sum += i * 2;
            }
        }
        return sum;
    }

    public int threeOfAKind() {
        return getSumOfGivenPairNumber(3);
    }

    public int fourOfAKind(){
        return getSumOfGivenPairNumber(4);
    }

    public int smallStraight() {
        if (Arrays.stream(dices.getTallies()).limit(5).allMatch(i -> i == 1)) {
            return 15;
        }
        return 0;
    }

    public int largeStraight() {
        if (Arrays.stream(dices.getTallies()).skip(1).allMatch(i -> i == 1)) {
            return 20;
        }
        return 0;
    }

    public int fullHouse() {
        int total = 0;
        int pair = 5;
        Map<Integer, List<Integer>> map = dices.getDice().stream().collect(Collectors.groupingBy(i -> i));
        if(map.keySet().size()>= 3){
            return 0;
        }
        for (int key: map.keySet()) {
            if(map.get(key).size() <= pair){
                pair -= map.get(key).size();
                total = map.get(key).stream().mapToInt(Integer::intValue).sum() + total;
            } else {
                return 0;
            }
        }
        return total;
    }
}



