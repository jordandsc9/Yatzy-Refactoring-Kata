import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class YatzyTest {

    @Test
    public void test_chance_scores_sum_of_all_dice() {
        assertEquals(15, new Yatzy(2,3,4,5,1).chance());
    }

    @Test
    public void test_yatzy_scores_50() {
        assertEquals(50, new Yatzy(6,6,6,6,6).yatzy());
    }

    @Test
    public void test_yatzy_scores_0() {
        assertEquals(0, new Yatzy(6,2,3,6,3).yatzy());
    }

    @Test
    public void test_ones_scores2() {
        assertEquals(2, new Yatzy(1,2,1,4,5).ones());
    }

    @Test
    public void test_ones_scores0() {
        assertEquals(0, new Yatzy(6,2,2,4,5).ones());
    }

    @Test
    public void test_twos_score10() {
        assertEquals(10, new Yatzy(2,2,2,2,2).twos());
    }

    @Test
    public void test_twos_score0() {
        assertEquals(0, new Yatzy(3,4,5,6,1).twos());
    }

    @Test
    public void test_threes_score12() {
        assertEquals(12, new Yatzy(2,3,3,3,3).threes());
    }

    @Test
    public void test_threes_score0() {
        assertEquals(0, new Yatzy(2,1,2,5,2).threes());
    }

    @Test
    public void test_fours_score12() {
        assertEquals(12, new Yatzy(4,4,4,5,5).fours());
    }

    @Test
    public void test_fours_score0() {
        assertEquals(0, new Yatzy(2,2,2,5,5).fours());
    }

    @Test
    public void test_fives_score10() {
        assertEquals(10, new Yatzy(4,4,4,5,5).fives());
    }

    @Test
    public void test_fives_score0() {
        assertEquals(0, new Yatzy(4,4,4,2,2).fives());
    }

    @Test
    public void test_sixes_score18() {
        assertEquals(18, new Yatzy(6,5,6,6,5).sixes());
    }

    @Test
    public void test_sixes_score0() {
        assertEquals(0, new Yatzy(4,4,4,5,5).sixes());
    }

    @Test
    public void test_one_pair_score12() {
        assertEquals(12, new Yatzy(5,3,6,6,5).scorePair());
    }

    @Test
    public void test_one_pair_score0() {
        assertEquals(0, new Yatzy(3,4,1,5,6).scorePair());
    }

    @Test
    public void test_two_Pair_score16() {
        assertEquals(16, new Yatzy(3,3,5,4,5).twoPair());
    }

    @Test
    public void test_two_Pair_score0() {
        assertEquals(0, new Yatzy(3,1,2,6,5).twoPair());
    }

    @Test
    public void test_three_of_a_kind_score15() {
        assertEquals(0, new Yatzy(3,1,2,3,5).threeOfAKind());
    }

    @Test
    public void test_three_of_a_kind_score0() {
        assertEquals(0, new Yatzy(3,1,2,3,5).threeOfAKind());
    }

    @Test
    public void test_four_of_a_kind_score20() {
        assertEquals(20, new Yatzy(5,5,5,4,5).fourOfAKind());
    }

    @Test
    public void test_four_of_a_kind_score0() {
        assertEquals(0, new Yatzy(3,3,5,2,3).fourOfAKind());
    }

    @Test
    public void test_smallStraight_score15() {
        assertEquals(15, new Yatzy(2,3,4,5,1).smallStraight());
    }

    @Test
    public void test_smallStraight_score0() {
        assertEquals(0, new Yatzy(1,2,2,4,5).smallStraight());
    }

    @Test
    public void test_largeStraight_score20() {
        assertEquals(20, new Yatzy(2,3,4,5,6).largeStraight());
    }

    @Test
    public void test_largeStraight_score0() {
        assertEquals(0, new Yatzy(1,2,2,4,5).largeStraight());
    }

    @Test
    public void test_fullHouse_score18() {
        assertEquals(18, new Yatzy(6,2,2,2,6).fullHouse());
    }

    @Test
    public void test_fullHouse_score0() {
        assertEquals(0, new Yatzy(2,3,4,5,6).fullHouse());
    }
}
