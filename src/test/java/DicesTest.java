import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DicesTest {

    @Test
    public void DicesTest(){
    Dices dices = new Dices(5,2,4,6,1);
    assertEquals(dices.getDice().get(1),2);
    }

    @Test
    public void TalliesCountTest(){
        Dices dices = new Dices(5,2,4,6,1);
        assertEquals(dices.getTallies()[0], 1);
    }

}

